package com.ykb.payments.bill.domain;

import static com.ykb.payments.bill.domain.utils.TestUtils.*;

import com.ykb.payments.creditcard.domain.CreditCard;
import com.ykb.payments.paymentorder.domain.PaymentOrder;
import com.ykb.payments.paymentorder.domain.PaymentOrderId;
import com.ykb.payments.paymentorder.domain.PaymentOrderStatus;
import org.junit.Test;

import java.util.List;

import static junit.framework.TestCase.assertEquals;

public class BillTest {

    @Test
    public void newly_created_bill_status_should_be_unpaid() {
        Bill bill = createBill();
        assertEquals(BillStatus.UNPAID, bill.getBillStatus());
    }

    @Test
    public void given_costumer_and_unpaid_bill_when_costumer_selects_credit_card_and_pays_unpaid_bill_then_bill_should_be_paid() throws InvalidCustomerId {
        Customer customer = createCustomer();
        Bill bill = createBill();
        List<CreditCard> creditCards = getCreditCards(customer);
        bill.pay(creditCards.get(0));
        assertEquals(BillStatus.PAID, bill.getBillStatus());
    }

    @Test
    public void given_payment_order_when_payment_order_is_canceled_then_order_status_should_be_canceled() throws InvalidCustomerId {
        PaymentOrder paymentOrder = createPaymentOrder();
        paymentOrder.cancel();
        assertEquals(PaymentOrderStatus.CANCELED, paymentOrder.getStatus());
    }

    @Test
    public void payment_order_should_be_active_when_payment_order_is_newly_created() throws InvalidCustomerId {
        PaymentOrder paymentOrder = createPaymentOrder();
        assertEquals(PaymentOrderStatus.ACTIVE, paymentOrder.getStatus());
    }

    private PaymentOrder createPaymentOrder() throws InvalidCustomerId {
        return PaymentOrder.create(PaymentOrderId.create("123123"), CustomerId.create("123123"), createSubscription(), createCreditCard());
    }
}
