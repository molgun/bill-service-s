package com.ykb.payments.bill.domain;

import org.junit.Test;

import static com.ykb.payments.bill.domain.utils.TestUtils.createCustomer;

/**
 * Created by molgun on 16/05/2017.
 */
public class CustomerTest {
    @Test(expected = InvalidCustomerId.class)
    public void given_wrong_customer_and_subscription_id_when_payment_order_is_given_then_payment_order_is_not_created() throws InvalidCustomerId {
        Customer customer = createCustomer(CustomerId.create("123123123123123"));
    }
}
