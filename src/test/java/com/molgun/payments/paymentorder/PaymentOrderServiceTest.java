package com.ykb.payments.paymentorder;

import com.ykb.payments.bill.domain.*;
import com.ykb.payments.creditcard.domain.CreditCard;
import com.ykb.payments.institution.domain.InstitutionCode;
import com.ykb.payments.paymentorder.domain.PaymentOrderAlreadyExists;
import com.ykb.payments.paymentorder.infrastructure.InMemoryPaymentOrderRepository;
import com.ykb.payments.paymentorder.application.PaymentOrderServiceImpl;
import org.junit.Assert;
import org.junit.Test;
import static com.ykb.payments.bill.domain.utils.TestUtils.*;

public class PaymentOrderServiceTest {

    @Test(expected = PaymentOrderAlreadyExists.class)
    public void given_customer_and_subscription_that_has_payment_order_when_payment_order_is_given_then_payment_order_is_not_created() throws SubscriptionAlreadyHasPaymentOrder, InvalidCustomerId, PaymentOrderAlreadyExists {
        Customer customer = createCustomer();
        Subscription subscription = Subscription.create(InstitutionCode.create("TRKCLL"), SubscriptionId.create("313232"));
        CreditCard creditCard = getCreditCards(customer).get(0);
        InMemoryPaymentOrderRepository inMemoryPaymentOrderRepository = createPaymentOrderRepository();
        PaymentOrderServiceImpl paymentOrderService = createPaymentOrderService(inMemoryPaymentOrderRepository);
        paymentOrderService.giveOrder(customer, subscription, creditCard);
    }
    @Test
    public void given_subscription_when_payment_order_is_given_then_payment_order_is_created() throws InvalidCustomerId, PaymentOrderAlreadyExists {
        Customer customer = createCustomer();
        Subscription subscription = createSubscription();
        CreditCard creditCard = getCreditCards(customer).get(0);
        InMemoryPaymentOrderRepository inMemoryPaymentOrderRepository = new InMemoryPaymentOrderRepository();
        PaymentOrderServiceImpl paymentService = createPaymentOrderService(inMemoryPaymentOrderRepository);
        paymentService.giveOrder(customer, subscription, creditCard);
        Assert.assertEquals(1, inMemoryPaymentOrderRepository.count());
    }

    @Test
    public void given_wrong_customer_and_subscription_id_when_payment_order_is_given_then_payment_order_is_not_created() {
    }
}
