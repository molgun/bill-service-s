package com.ykb.payments.paymentorder.application;

import com.ykb.payments.bill.domain.*;
import com.ykb.payments.bill.infrastructure.H2BillRepository;
import com.ykb.payments.creditcard.domain.CreditCard;
import com.ykb.payments.institution.domain.InstitutionCode;
import com.ykb.payments.paymentorder.domain.PaymentOrder;
import com.ykb.payments.paymentorder.domain.PaymentOrderAlreadyExists;
import com.ykb.payments.paymentorder.domain.PaymentOrderId;
import com.ykb.payments.paymentorder.infrastructure.H2PaymentOrderRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static com.ykb.payments.bill.domain.utils.TestUtils.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class PaymentOrderServiceIT {

    @Autowired
    H2BillRepository billRepository;

    @Autowired
    H2PaymentOrderRepository paymentOrderRepository;

    @Before
    public void init() throws InvalidCustomerId {
        billRepository.deleteAll();
        paymentOrderRepository.deleteAll();
        Subscription subscription = Subscription.create(InstitutionCode.create("TRKCLL"), SubscriptionId.create("313232"));
        Subscription subscription2 = Subscription.create(InstitutionCode.create("TRKCLL"), SubscriptionId.create("232324"));
        billRepository.save(new Bill(createBillId(), subscription, createAmount(), createDueDate()));
        billRepository.save(new Bill(createBillId("142142"), subscription2, createAmount(), createDueDate()));

        PaymentOrder order = PaymentOrder.create(PaymentOrderId.create("123122"), CustomerId.create("123123"),
                Subscription.create(InstitutionCode.create("TRKCLL"), SubscriptionId.create("313232")),
                createCreditCard());
        paymentOrderRepository.save(order);
    }

    @Test(expected = PaymentOrderAlreadyExists.class)
    public void given_customer_and_subscription_that_has_payment_order_when_payment_order_is_given_then_payment_order_is_not_created() throws SubscriptionAlreadyHasPaymentOrder, InvalidCustomerId, PaymentOrderAlreadyExists {
        Customer customer = createCustomer();
        Subscription subscription = Subscription.create(InstitutionCode.create("TRKCLL"), SubscriptionId.create("313232"));
        CreditCard creditCard = createCreditCard();
        PaymentOrderServiceImpl paymentOrderService = new PaymentOrderServiceImpl(paymentOrderRepository);
        paymentOrderService.giveOrder(customer, subscription, creditCard);
    }

    @Test
    public void given_subscription_when_payment_order_is_given_then_payment_order_is_created() throws InvalidCustomerId, PaymentOrderAlreadyExists {
        Customer customer = createCustomer();
        Subscription subscription = createSubscription();
        CreditCard creditCard = getCreditCards(customer).get(0);
        PaymentOrderServiceImpl paymentService = new PaymentOrderServiceImpl(paymentOrderRepository);
        paymentService.giveOrder(customer, subscription, creditCard);
        Assert.assertEquals(2, paymentOrderRepository.count());
    }
}
