package com.ykb.payments.paymentorder.application;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class PaymentOrderControllerTest {

    @Autowired
    private TestRestTemplate restTemplate;

    PaymentOrderRequestDTO dto;

    @Before
    public void init() {
        dto = new PaymentOrderRequestDTO();
        dto.setCustomerId("123123");
        dto.setCardNumber("234234");
        dto.setInstitutionCode("TRKCLL");
        dto.setSubscriptionId("5051232323");
    }

    @Test
    public void shouldReturnAnEmptyList() {
        String body = this.restTemplate.getForObject("/payment_orders/credit_card/123123", String.class);
        assertThat(body).isEqualTo("[]");
    }

    @Test
    public void shouldGiveAPaymentOrder() {
        ResponseEntity<String> result = this.restTemplate.postForEntity("/payment_orders/give_order", dto, String.class);
        assertThat(result.getStatusCode().is2xxSuccessful()).isEqualTo(true);
    }

    @Test
    public void shouldReturnPaymentOrderAlreadyExist() {
        ResponseEntity<String> result = this.restTemplate.postForEntity("/payment_orders/give_order", dto, String.class);
        assertThat(result.getStatusCode().is4xxClientError()).isEqualTo(true);
    }

    @Test
    public void shouldReturnPaymentOrdersByCreditCard() {
        String creditCard = dto.getCardNumber();
        PaymentOrderRequestDTO[] response = this.restTemplate.getForObject("/payment_orders/credit_card/" + creditCard, PaymentOrderRequestDTO[].class);
        assertThat(response.length).isEqualTo(1);
        assertThat(response[0]).isEqualTo(dto);
    }
}
