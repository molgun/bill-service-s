package com.ykb.payments.paymentorder.infrastructure;

import com.ykb.payments.bill.domain.*;
import com.ykb.payments.creditcard.domain.CreditCard;
import com.ykb.payments.institution.domain.InstitutionCode;
import com.ykb.payments.paymentorder.domain.PaymentOrder;
import com.ykb.payments.paymentorder.domain.PaymentOrderId;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class H2PaymentOrderRepositoryIT {

    @Autowired
    H2PaymentOrderRepository paymentOrderRepository;

    @Before
    public void init() throws InvalidCustomerId {
        PaymentOrder order = PaymentOrder.create(PaymentOrderId.create("123122"), CustomerId.create("123123"),
                Subscription.create(InstitutionCode.create("TRKCLL"), SubscriptionId.create("313232")),
                new CreditCard("123123"));
        paymentOrderRepository.save(order);

        PaymentOrder order2 = PaymentOrder.create(PaymentOrderId.create("1231221"), CustomerId.create("123123"),
                Subscription.create(InstitutionCode.create("TRKCLL"), SubscriptionId.create("3132323")),
                new CreditCard("123123"));
        paymentOrderRepository.save(order2);
    }

    @Test
    public void shouldReturnPaymentOrdersByCreditCard() {
        CreditCard cc = new CreditCard("123123");
        int size = paymentOrderRepository.findByCreditCard(cc).size();
        Assert.assertEquals(2, size);
    }
}
