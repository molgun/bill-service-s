package com.ykb.payments.bill.application;

import com.ykb.payments.bill.domain.Bill;
import com.ykb.payments.creditcard.domain.CreditCard;
import com.ykb.payments.creditcard.domain.CreditCardService;
import com.ykb.payments.creditcard.domain.TrackingId;

public class PaymentEventsImpl implements PaymentEvents {

    private CreditCardService creditCardService;

    public PaymentEventsImpl(CreditCardService creditCardService) {
        this.creditCardService = creditCardService;
    }

    @Override
    public void paymentRequested() {

    }

    @Override
    public void paid(Bill bill, CreditCard paymentSource) {
        TrackingId trackingId = new TrackingId(bill.getBillId().getId());
        creditCardService.takeProvision(paymentSource, bill.getAmount(), trackingId);
    }

    @Override
    public void paidBillCanceled(Bill bill) {

    }
}
