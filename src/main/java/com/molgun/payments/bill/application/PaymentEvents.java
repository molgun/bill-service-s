package com.ykb.payments.bill.application;

import com.ykb.payments.bill.domain.Bill;
import com.ykb.payments.creditcard.domain.CreditCard;

/**
 * Created by molgun on 16/05/2017.
 */
public interface PaymentEvents {

    public void paymentRequested();

    public void paid(Bill bill, CreditCard paymentSource);

    public void paidBillCanceled(Bill bill);
}
