package com.ykb.payments.bill.domain;

import com.ykb.payments.institution.domain.InstitutionCode;
import org.apache.commons.lang3.Validate;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

public class Subscription {

    public Subscription() {
    }

    private InstitutionCode institutionCode;
    private SubscriptionId subscriptionId;

    public Subscription(InstitutionCode institutionCode, SubscriptionId subscriptionId) {
        this.institutionCode = institutionCode;
        this.subscriptionId = subscriptionId;
    }

    public static Subscription create(InstitutionCode institutionCode, SubscriptionId subscriptionId) {
        Validate.notNull(institutionCode);
        Validate.notNull(subscriptionId);
        return new Subscription(institutionCode, subscriptionId);
    }

    public InstitutionCode getInstitutionCode() {
        return institutionCode;
    }

    public SubscriptionId getSubscriptionId() {
        return subscriptionId;
    }


    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .append(institutionCode)
                .append(subscriptionId)
                .toHashCode();
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        final Subscription subscription = (Subscription) o;

        return sameValueAs(subscription);
    }

    public boolean sameValueAs(Subscription subscription) {
        return subscription != null && new EqualsBuilder()
                .append(institutionCode, subscription.getInstitutionCode())
                .append(subscriptionId, subscription.getSubscriptionId())
                .isEquals();
    }
}
