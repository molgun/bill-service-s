package com.ykb.payments.bill.domain;

/**
 * Created by molgun on 16/05/2017.
 */
public enum BillStatus {
    UNPAID, PAID, PAYMENT_REQUESTED
}
