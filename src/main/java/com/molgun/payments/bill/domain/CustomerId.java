package com.ykb.payments.bill.domain;

/**
 * Created by molgun on 16/05/2017.
 */
public class CustomerId {

    private String id;

    public CustomerId() {
    }

    private CustomerId(String id) {
        this.id = id;
    }

    public static CustomerId create(String id) throws InvalidCustomerId {
        if (id.length() > 8) {
            throw new InvalidCustomerId();
        }
        return new CustomerId(id);
    }

    public String getId() {
        return id;
    }
}
