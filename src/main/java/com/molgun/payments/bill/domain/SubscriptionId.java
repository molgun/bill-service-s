package com.ykb.payments.bill.domain;

import org.apache.commons.lang3.Validate;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

public class SubscriptionId {

    private String id;

    public SubscriptionId(String id) {
        this.id = id;
    }

    public SubscriptionId() {
    }

    public static SubscriptionId create(String id) {
        Validate.notNull(id);
        return new SubscriptionId(id);
    }

    public String getId() {
        return id;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .append(id)
                .toHashCode();
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        final SubscriptionId subscriptionId = (SubscriptionId) o;

        return sameValueAs(subscriptionId);
    }

    public boolean sameValueAs(SubscriptionId subscription) {
        return subscription != null && new EqualsBuilder()
                .append(id, subscription.getId())
                .isEquals();
    }
}
