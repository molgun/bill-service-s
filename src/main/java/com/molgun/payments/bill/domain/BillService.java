package com.ykb.payments.bill.domain;

import com.ykb.payments.bill.application.PaymentEvents;
import com.ykb.payments.creditcard.domain.CreditCard;
import com.ykb.payments.creditcard.domain.ProvisionException;
import com.ykb.payments.paymentorder.domain.PaymentOrder;
import com.ykb.payments.paymentorder.domain.PaymentOrderRepository;
import org.springframework.transaction.annotation.Transactional;

import javax.naming.OperationNotSupportedException;
import java.util.Date;
import java.util.List;

public class BillService {

    private BillRepository billRepository;
    private PaymentOrderRepository paymentOrderRepository;
    private PaymentEvents paymentEvents;

    public BillService(BillRepository billRepository,
                       PaymentOrderRepository paymentOrderRepository,
                       PaymentEvents paymentEvents) {
        this.billRepository = billRepository;
        this.paymentOrderRepository = paymentOrderRepository;
        this.paymentEvents = paymentEvents;
    }

    @Transactional
    public void pay(Bill bill, CreditCard paymentSource) throws OperationNotSupportedException {
        bill.pay(paymentSource);
        billRepository.save(bill);
        paymentEvents.paid(bill, paymentSource);
    }

    public void payDueBills() throws ProvisionException, OperationNotSupportedException {
        List<Bill> bills = billRepository.findByDueDate(new DueDate(new Date()));
        for (Bill bill : bills) {
            PaymentOrder paymentOrder = paymentOrderRepository.findBySubscription(bill.getSubscription());
            if (paymentOrder != null) {
                pay(bill, paymentOrder.getCreditCard());
            }
        }
    }
}
