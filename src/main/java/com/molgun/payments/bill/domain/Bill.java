package com.ykb.payments.bill.domain;

import com.ykb.payments.creditcard.domain.CreditCard;
import org.apache.commons.lang3.Validate;

import java.util.Collections;
import java.util.List;

public class Bill {

    private BillId billId;
    private BillStatus billStatus;
    private Subscription subscription;
    private Amount amount;
    private DueDate dueDate;

    Bill() {
    }

    public Bill(BillId billId, Subscription subscription, Amount amount, DueDate dueDate) {
        Validate.notNull(subscription);
        Validate.notNull(billId);
        Validate.notNull(amount);
        Validate.notNull(dueDate);
        this.billId = billId;
        this.billStatus = BillStatus.UNPAID;
        this.subscription = subscription;
        this.amount = amount;
        this.dueDate = dueDate;
    }

    public void pay(CreditCard creditCard) {
        this.billStatus = BillStatus.PAID;
    }


    public BillStatus getBillStatus() {
        return billStatus;
    }

    public BillId getBillId() {
        return billId;
    }

    public Amount getAmount() {
        return amount;
    }

    public DueDate getDueDate() {
        return dueDate;
    }

    public Subscription getSubscription() {
        return subscription;
    }
}
