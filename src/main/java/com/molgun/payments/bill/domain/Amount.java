package com.ykb.payments.bill.domain;

import java.math.BigDecimal;

/**
 * Created by molgun on 17/05/2017.
 */
public class Amount {
    private BigDecimal amount;

    public Amount() {
    }

    public Amount(BigDecimal amount) {
        this.amount = amount;
    }

    public BigDecimal getAmount() {
        return amount;
    }
}
