package com.ykb.payments.bill.domain;

import org.apache.commons.lang3.Validate;

/**
 * Created by molgun on 16/05/2017.
 */
public class Customer {
    private CustomerId customerId;

    private Customer(CustomerId customerId) {
        this.customerId = customerId;
    }

    public static Customer create(CustomerId customerId) {
        Validate.notNull(customerId);
        return new Customer(customerId);
    }

    public CustomerId getCustomerId() {
        return customerId;
    }
}
