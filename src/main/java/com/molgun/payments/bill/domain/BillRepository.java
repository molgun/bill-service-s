package com.ykb.payments.bill.domain;

import java.util.List;

/**
 * Created by molgun on 17/05/2017.
 */
public interface BillRepository {

    public Bill save(Bill bill);

    public Bill findOne(BillId billId);

    public List<Bill> findByDueDate(DueDate dueDate);

    public long count();

    public long countByBillStatus(BillStatus status);
}
