package com.ykb.payments.bill.infrastructure;

import com.ykb.payments.bill.domain.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


public class InMemoryBillRepository implements com.ykb.payments.bill.domain.BillRepository {

    private Map<BillId, Bill> bills;

    public InMemoryBillRepository() {
        this.bills = new HashMap<>();
    }

    public Bill save(Bill bill) {
        bills.put(bill.getBillId(), bill);
        return bill;
    }

    public Bill findOne(BillId billId) {
        return bills.get(billId);
    }

    public List<Bill> findByDueDate(DueDate dueDate) {
        List<Bill> fetchedBills = bills.values()
                .stream().filter((o) -> o.getDueDate().equals(dueDate))
                .collect(Collectors.toList());
        return fetchedBills;
    }

    public long count() {
        return bills.size();
    }

    public long countByBillStatus(BillStatus status) {
        return bills.values()
                .stream().filter((o) -> o.getBillStatus().equals(status))
                .collect(Collectors.toList())
                .size();
    }
}
