package com.ykb.payments.creditcard.domain;

import com.ykb.payments.bill.domain.BillId;
import com.ykb.payments.bill.domain.BillService;

public class CreditCardEventsImpl implements CreditCardEvents {

    private BillService billService;

    public CreditCardEventsImpl(BillService billService) {
        this.billService = billService;
    }

    @Override
    public void couldNotProvisioned(TrackingId trackingId) {
    }
}
