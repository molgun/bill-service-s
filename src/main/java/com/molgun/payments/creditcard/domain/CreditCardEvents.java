package com.ykb.payments.creditcard.domain;

public interface CreditCardEvents {
    public void couldNotProvisioned(TrackingId trackingId);
}
