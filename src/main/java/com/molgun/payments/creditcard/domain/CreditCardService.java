package com.ykb.payments.creditcard.domain;

import com.ykb.payments.bill.domain.Amount;

/**
 * Created by molgun on 18/05/2017.
 */
public interface CreditCardService {

    public void takeProvision(CreditCard creditCard, Amount amount, TrackingId trackingId);
}
