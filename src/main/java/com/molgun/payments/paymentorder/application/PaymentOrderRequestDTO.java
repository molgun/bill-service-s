package com.ykb.payments.paymentorder.application;

import com.ykb.payments.bill.domain.*;
import com.ykb.payments.creditcard.domain.CreditCard;
import com.ykb.payments.institution.domain.InstitutionCode;
import com.ykb.payments.paymentorder.domain.PaymentOrder;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

public class PaymentOrderRequestDTO {

    private String customerId;
    private String institutionCode;
    private String subscriptionId;
    private String cardNumber;

    public PaymentOrderRequestDTO() {
    }

    public PaymentOrderRequestDTO fromPaymentOrder(PaymentOrder paymentOrder) {
        customerId = paymentOrder.getCustomerId().getId();
        institutionCode = paymentOrder.getSubscription().getInstitutionCode().getInstitutionCode();
        subscriptionId = paymentOrder.getSubscription().getSubscriptionId().getId();
        cardNumber = paymentOrder.getCreditCard().getCardNumber();
        return this;
    }

    public Subscription asSubscription() {
        InstitutionCode ic = InstitutionCode.create(getInstitutionCode());
        SubscriptionId si = SubscriptionId.create(getSubscriptionId());
        return Subscription.create(ic, si);
    }

    public Customer asCustomer() throws InvalidCustomerId {
        return Customer.create(CustomerId.create(getCustomerId()));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        PaymentOrderRequestDTO that = (PaymentOrderRequestDTO) o;

        return new EqualsBuilder()
                .append(customerId, that.customerId)
                .append(institutionCode, that.institutionCode)
                .append(subscriptionId, that.subscriptionId)
                .append(cardNumber, that.cardNumber)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .append(customerId)
                .append(institutionCode)
                .append(subscriptionId)
                .append(cardNumber)
                .toHashCode();
    }

    public CreditCard asCreditCard () {
        return new CreditCard(getCardNumber());
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getInstitutionCode() {
        return institutionCode;
    }

    public void setInstitutionCode(String institutionCode) {
        this.institutionCode = institutionCode;
    }

    public String getSubscriptionId() {
        return subscriptionId;
    }

    public void setSubscriptionId(String subscriptionId) {
        this.subscriptionId = subscriptionId;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }
}
