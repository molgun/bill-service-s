package com.ykb.payments.paymentorder.application;

import com.ykb.payments.bill.domain.*;
import com.ykb.payments.creditcard.domain.CreditCard;
import com.ykb.payments.paymentorder.domain.PaymentOrder;
import com.ykb.payments.paymentorder.domain.PaymentOrderAlreadyExists;
import com.ykb.payments.paymentorder.domain.PaymentOrderService;
import com.ykb.payments.paymentorder.infrastructure.H2PaymentOrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by molgun on 18/05/2017.
 */
@RestController
public class PaymentOrderController {

    @Autowired
    H2PaymentOrderRepository h2PaymentOrderRepository;

    PaymentOrderService paymentOrderService;

    @PostConstruct
    public void init() {
        this.paymentOrderService = new PaymentOrderServiceImpl(h2PaymentOrderRepository);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/payment_orders/give_order")
    public ResponseEntity giveOrder(@RequestBody PaymentOrderRequestDTO dto) {
        try {
            paymentOrderService.giveOrder(dto.asCustomer(), dto.asSubscription(), dto.asCreditCard());
            return ResponseEntity.ok().build();
        } catch (PaymentOrderAlreadyExists paymentOrderAlreadyExists) {
            return ResponseEntity.status(HttpStatus.CONFLICT).body(paymentOrderAlreadyExists.getMessage());
        } catch (InvalidCustomerId invalidCustomerId) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(invalidCustomerId.getMessage());
        }
    }

    @RequestMapping("/payment_orders/credit_card/{creditCardNumber}")
    public ResponseEntity paymentOrderList(@PathVariable String creditCardNumber) {
        CreditCard creditCard = new CreditCard(creditCardNumber);
        List<PaymentOrder> paymentOrders = h2PaymentOrderRepository.findByCreditCard(creditCard);
        return ResponseEntity
                .ok(paymentOrders
                        .stream()
                        .map((po) -> new PaymentOrderRequestDTO()
                                .fromPaymentOrder(po)
                        ).collect(Collectors.toList())
                );
    }
}
