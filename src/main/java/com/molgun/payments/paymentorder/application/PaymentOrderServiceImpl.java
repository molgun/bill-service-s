package com.ykb.payments.paymentorder.application;

import com.ykb.payments.creditcard.domain.CreditCard;
import com.ykb.payments.bill.domain.Customer;
import com.ykb.payments.bill.domain.Subscription;
import com.ykb.payments.paymentorder.domain.*;

import javax.transaction.Transactional;
import java.util.UUID;


public class PaymentOrderServiceImpl implements PaymentOrderService {

    private final PaymentOrderRepository repository;

    public PaymentOrderServiceImpl(PaymentOrderRepository repository) {
        this.repository = repository;
    }

    @Transactional
    public void giveOrder(Customer customer, Subscription subscription, CreditCard creditCard) throws PaymentOrderAlreadyExists {
        PaymentOrder paymentOrder = repository.findBySubscription(subscription);
        if (paymentOrder != null) {
            throw new PaymentOrderAlreadyExists();
        }

        PaymentOrder newOrder = PaymentOrder.create(PaymentOrderId.create(UUID.randomUUID().toString()), customer.getCustomerId(), subscription, creditCard);
        repository.save(newOrder);
    }
}
