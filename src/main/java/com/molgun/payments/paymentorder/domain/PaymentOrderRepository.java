package com.ykb.payments.paymentorder.domain;

import com.ykb.payments.creditcard.domain.CreditCard;
import com.ykb.payments.bill.domain.Subscription;

import java.util.List;

/**
 * Created by molgun on 17/05/2017.
 */
public interface PaymentOrderRepository {
    public PaymentOrder save(PaymentOrder paymentOrder);

    public PaymentOrder findOne(PaymentOrderId paymentOrderId);

    public PaymentOrder findBySubscription(Subscription subscription);

    public long count();

    public List<PaymentOrder> findByCreditCard(CreditCard creditCard);
}
