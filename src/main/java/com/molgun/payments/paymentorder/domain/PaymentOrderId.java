package com.ykb.payments.paymentorder.domain;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.io.Serializable;

/**
 * Created by molgun on 17/05/2017.
 */
public class PaymentOrderId implements Serializable {

    private String id;

    public PaymentOrderId() {
    }

    private PaymentOrderId(String id) {
        this.id = id;
    }

    public static PaymentOrderId create(String id) {
        return new PaymentOrderId(id);
    }

    public String getId() {
        return id;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .append(id)
                .toHashCode();
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        final PaymentOrderId paymentOrderId = (PaymentOrderId) o;

        return sameValueAs(paymentOrderId);
    }

    public boolean sameValueAs(PaymentOrderId paymentOrderId) {
        return paymentOrderId != null && new EqualsBuilder()
                .append(id, paymentOrderId.getId())
                .isEquals();
    }
}
