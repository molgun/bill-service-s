package com.ykb.payments.paymentorder.domain;

import com.ykb.payments.creditcard.domain.CreditCard;
import com.ykb.payments.bill.domain.CustomerId;
import com.ykb.payments.bill.domain.Subscription;
import org.apache.commons.lang3.Validate;


/**
 * Created by molgun on 16/05/2017.
 */

public class PaymentOrder {

    private PaymentOrderId paymentOrderId;
    private Subscription subscription;
    private PaymentOrderStatus status;
    private CreditCard creditCard;
    private CustomerId customerId;

    private PaymentOrder(PaymentOrderId paymentOrderId, CustomerId customerId, Subscription subscription, CreditCard creditCard) {
        this.paymentOrderId = paymentOrderId;
        this.subscription = subscription;
        this.customerId = customerId;
        this.creditCard = creditCard;
        this.status = PaymentOrderStatus.ACTIVE;
    }

    public PaymentOrder() {
    }

    public static PaymentOrder create(PaymentOrderId paymentOrderId,CustomerId customerId, Subscription subscription, CreditCard creditCard) {
        Validate.notNull(subscription);
        Validate.notNull(customerId);
        Validate.notNull(creditCard);
        return new PaymentOrder(paymentOrderId, customerId, subscription, creditCard);
    }

    public PaymentOrderId getPaymentOrderId() {
        return paymentOrderId;
    }

    public Subscription getSubscription() {
        return subscription;
    }

    public PaymentOrderStatus getStatus() {
        return status;
    }

    public void cancel() {
        this.status = PaymentOrderStatus.CANCELED;
    }

    public CreditCard getCreditCard() {
        return creditCard;
    }

    public CustomerId getCustomerId() {
        return customerId;
    }
}
