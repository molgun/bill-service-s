package com.ykb.payments.paymentorder.domain;

/**
 * Created by molgun on 16/05/2017.
 */
public interface PaymentOrderEvents {

    public void paymentOrderCanceled();

    public void paymentOrderIsPlaced();

    public void paymentOrderUpdated();

}
