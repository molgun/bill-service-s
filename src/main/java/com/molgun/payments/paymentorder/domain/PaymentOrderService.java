package com.ykb.payments.paymentorder.domain;

import com.ykb.payments.creditcard.domain.CreditCard;
import com.ykb.payments.bill.domain.Customer;
import com.ykb.payments.bill.domain.Subscription;

/**
 * Created by molgun on 18/05/2017.
 */
public interface PaymentOrderService {

    public void giveOrder(Customer customer, Subscription subscription, CreditCard creditCard) throws PaymentOrderAlreadyExists;
}
