package com.ykb.payments.paymentorder.infrastructure;

import com.ykb.payments.creditcard.domain.CreditCard;
import com.ykb.payments.bill.domain.Subscription;
import com.ykb.payments.paymentorder.domain.PaymentOrder;
import com.ykb.payments.paymentorder.domain.PaymentOrderId;
import com.ykb.payments.paymentorder.domain.PaymentOrderRepository;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by molgun on 17/05/2017.
 */
public interface H2PaymentOrderRepository extends CrudRepository<PaymentOrder, PaymentOrderId>, PaymentOrderRepository {

    public PaymentOrder save(PaymentOrder paymentOrder);

    public PaymentOrder findOne(PaymentOrderId paymentOrderId);

    public PaymentOrder findBySubscription(Subscription subscription);

    public long count();

    public List<PaymentOrder> findByCreditCard(CreditCard creditCard);
}
