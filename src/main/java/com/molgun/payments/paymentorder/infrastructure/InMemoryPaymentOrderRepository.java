package com.ykb.payments.paymentorder.infrastructure;

import com.ykb.payments.creditcard.domain.CreditCard;
import com.ykb.payments.bill.domain.Subscription;
import com.ykb.payments.paymentorder.domain.PaymentOrder;
import com.ykb.payments.paymentorder.domain.PaymentOrderId;
import com.ykb.payments.paymentorder.domain.PaymentOrderRepository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class InMemoryPaymentOrderRepository implements PaymentOrderRepository {

    private Map<PaymentOrderId, PaymentOrder> paymentOrders;

    public InMemoryPaymentOrderRepository() {
        this.paymentOrders = new HashMap<>();
    }

    public PaymentOrder save(PaymentOrder paymentOrder) {
        paymentOrders.put(paymentOrder.getPaymentOrderId(), paymentOrder);
        return paymentOrder;
    }

    public PaymentOrder findOne(PaymentOrderId paymentOrderId) {
        return paymentOrders.get(paymentOrderId);
    }

    public List<PaymentOrder> findByCreditCard(CreditCard creditCard) {
        List<PaymentOrder> orders =
                paymentOrders
                        .values()
                        .stream()
                        .filter((o) -> o.getCreditCard().equals(creditCard))
                        .collect(Collectors.toList());
        if (orders.isEmpty()) {
            return null;
        }
        return orders;
    }

    public PaymentOrder findBySubscription(Subscription subscription) {
        List<PaymentOrder> orders =
                paymentOrders
                        .values()
                        .stream()
                        .filter((o) -> o.getSubscription().equals(subscription))
                        .collect(Collectors.toList());
        if (orders.isEmpty()) {
            return null;
        }
        return orders.get(0);
    }

    public long count() {
        return paymentOrders.size();
    }
}
