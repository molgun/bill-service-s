package com.ykb.payments.institution.domain;

/**
 * Created by molgun on 16/05/2017.
 */
public interface InstitutionEvents {
    public void institutionUpdated();

    public void institutionPassivated();

    public void paymentOrderDeclared();
}
