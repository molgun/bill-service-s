package com.ykb.payments.institution.domain;

import org.apache.commons.lang3.Validate;

/**
 * Created by molgun on 16/05/2017.
 */
public class Institution {
    private InstitutionCode instutitionCode;

    public Institution() {
    }

    public Institution(InstitutionCode institutionCode) {
        Validate.notNull(institutionCode);
        this.instutitionCode = institutionCode;
    }

    public InstitutionCode getInstutitionCode() {
        return instutitionCode;
    }
}
